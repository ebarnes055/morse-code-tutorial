
/*
 * This code was adapted from: https://programmingelectronics.com/wp-content/uploads/2015/01/Multiple-Button-Press-Options.pdf 
 * and https://core-electronics.com.au/tutorials/use-lcd-arduino-uno.html
 *
*/
// Import the Liquid Crystal library
#include <LiquidCrystal.h>;

/*Declare and initalise variables*/
//Counters
float PRESS_LEN_MS=0;
float PRESS_LEN_MS_RED=0;
int OP_1_MS =100;
int OP_2_MS=700;
int INIT_COUNT=0;

//Button and PIN inputs
int BUTTON_PIN=2;
int BUTTON_PIN_RED=7;
int LED_PIN_1=13; //RED
int LED_PIN_2=12; //GREEN

//Initialise the LCD with the arduino. LiquidCrystal(rs, enable, d4, d5, d6, d7)
LiquidCrystal lcd(8, 11, 6, 5, 4, 3);

/*FUNCTION DECLARATIONS*/
//Clear the screen on the first button press or when INIT_COUNT has been reset to 0
void clearScreen(){
  if (INIT_COUNT==1){lcd.clear();}
}

//Used to print to LCD with 16x2 grid, move to start of second line once counter>16
void checkCount(){
  if(INIT_COUNT>16){
    Serial.println(INIT_COUNT);
    lcd.setCursor((INIT_COUNT-17), 1); //EG: first case 15-14 (0,1)
  }
}

/*MAIN*/
void setup() {
   //Init push button pin as INPUT_PULLUP - 'this enables the internal pullup resistors'
    pinMode(BUTTON_PIN,  INPUT_PULLUP);
    pinMode(BUTTON_PIN_RED,  INPUT_PULLUP);

    //Set LEDs as outputs
    pinMode(LED_PIN_1, OUTPUT);
    pinMode(LED_PIN_2, OUTPUT);

  // Switch on the LCD screen and establish it as a 16x2 grid
    lcd.begin(16, 2);
  // Print to LCD screen
   lcd.print("BEGIN MORSE CODE"); 

    //Start serial comm - used for debugging
    Serial.begin(9600);
} 

/*This code was adapted from: https://programmingelectronics.com/wp-content/uploads/2015/01/Multiple-Button-Press-Options.pdf */
void loop() {
  /*Measure how long the button has been pressed for [10th of a second]*/
  /*Blue button*/
  while (digitalRead(BUTTON_PIN)==LOW){
    delay(100); 
    /*create a timer, PRESS_LEN_MS is initalised to 0, 
    incrementally add to it while button is held down*/
    PRESS_LEN_MS = PRESS_LEN_MS +100;
  }
  /*Red button*/
  while (digitalRead(BUTTON_PIN_RED)==LOW){
    delay(100); 
    PRESS_LEN_MS_RED = PRESS_LEN_MS_RED +100; //delay(100) 0+100, MS counter
  }

/*Check which button was pressed, perform action accordingly*/
  /*BLUE BUTTON '.' and '-' input*/
  if(PRESS_LEN_MS>=OP_2_MS){ //LONG PRESS >= 700ms
    
    INIT_COUNT+=1; //PRESS - increment counter
    digitalWrite(LED_PIN_2, HIGH); //GREEN ON
    digitalWrite(LED_PIN_1, LOW); //RED OFF
    
    clearScreen();
    //check the position of the cursor is <16, else go to next line
    checkCount(); 
    
    lcd.print('-');
    
  }else if(PRESS_LEN_MS==200){ //SHORT PRESS - in+out = 200ms
    
    INIT_COUNT+=1; //PRESS
    digitalWrite(LED_PIN_1, HIGH); //RED ON
    digitalWrite(LED_PIN_2, LOW); //GREEN OFF
    
    clearScreen();
    checkCount();
    lcd.print('.'); 
  } 

  /*Red button ' ' and clear function*/
  if (PRESS_LEN_MS_RED==200){
    INIT_COUNT+=1; //SHORT PRESS - ' '
    
    //incl. to prevent counter issue if space pressed first
    clearScreen(); 
    checkCount();
    
    lcd.print(' '); //SPACE
    
  }else if(PRESS_LEN_MS_RED>=OP_2_MS){ //LONG PRESS - clear screen
    //Move cursor to first row, first column
    lcd.setCursor(0, 1);
    //reset screen display
    lcd.clear();
    lcd.print("BEGIN MORSE CODE"); 
    //reset counter
    INIT_COUNT=0;
  }

  //Button has been lifted - reset press length for the next button press
  PRESS_LEN_MS=0;
  PRESS_LEN_MS_RED=0;
}
